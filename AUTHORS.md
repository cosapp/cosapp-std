# Credits

## Development Lead

* [Etienne Lac](https://gitlab.com/etienne.lac) (etienne.lac@safrangroup.com)

## Contributors

* Akka Technologies (https://www.akka-technologies.com)
* [Peter Cairns](https://gitlab.com/petercairns_akka) (peter.cairns@akka.eu)
* [André Hochschultz](https://gitlab.com/ahschulz2) (andre.hochschultz@akka.eu)
* [Juraj Perenyi](https://gitlab.com/JPerenyi) (juraj.perenyi@akka.eu)
* [Guy De Spiegeleer](https://gitlab.com/GuyDS)
