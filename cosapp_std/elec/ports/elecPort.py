from cosapp.ports import Port


class ElecPort(Port):
    """Electrical port containing current and voltage

    Variables:
    ----------
    - V: Electric potential [V]
    - I: Current intensity [A]
    """
    def setup(self):
        self.add_variable('V', 0., unit='V', dtype=(float, int), desc='Potential')
        self.add_variable('I', 0., unit='A', dtype=(float, int), desc='Current')
