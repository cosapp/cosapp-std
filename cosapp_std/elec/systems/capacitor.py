from cosapp.systems import System
from cosapp_std.elec.ports import ElecPort


class Capacitor(System):
    """Capacitor component

    Attributes:
        C : float
            Capacitance in Farads
    """

    def setup(self, C=0.):
        self.add_input(ElecPort, 'elec_in')
        self.add_output(ElecPort, 'elec_out')
        self.add_inward('C', abs(float(C)), unit='F', desc='Capacitance')
        self.add_outward('I', 0., unit='amp', desc='Current across capacitor')
        self.add_outward('deltaV')
        self.add_transient('Q', der='I')

    def compute(self):
        if self.elec_in.I == 0:
            self.elec_out.I = self.elec_in.I
            self.elec_out.V = self.elec_in.V

        else:

            self.I = self.elec_in.I

            # compute capacitor potential difference
            self.deltaV = (self.Q / self.C)

            # send out overall potential difference
            self.elec_out.V = self.elec_in.V - self.deltaV

            # pass through current
            self.elec_out.I = self.elec_in.I
