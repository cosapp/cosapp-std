from cosapp.systems import System
from cosapp_std.elec.ports import ElecPort
from cosapp.ports import units
from typing import Type
import numpy
import abc


RV_MIN = 1.0e-10  # diode resistance for +ve voltage
RV_MAX = 1.0e10   # diode resistance for -ve voltage


class AbstractDiodeModel(System):
    """Abstract base diode model"""
    def setup(self):
        self.add_input(ElecPort, 'elec_in')
        self.add_output(ElecPort, 'elec_out')

        self.add_outward('Rv', 0.0)

    @abc.abstractmethod
    def compute_Rv(self) -> None:
        """Computation of `self.Rv` outward"""
        pass

    def compute(self):
        self.compute_Rv()
        # send out voltages and currents
        self.elec_out.I = self.elec_in.I
        self.elec_out.V = self.elec_in.V - (self.elec_out.I * self.Rv)


class PiecewiseDiode(AbstractDiodeModel):
    """Piecewise linear diode resistance model
    """
    def compute_Rv(self) -> None:
        self.Rv = RV_MIN if self.elec_in.V > 0 else RV_MAX


class HyperbolicDiode(AbstractDiodeModel):
    """Hyperbolic tangent diode resistance model
    """
    def compute_Rv(self) -> None:
        self.Rv = 0.5 * (numpy.tanh(5 * (0.7 - self.elec_in.V)) + 1) * RV_MAX


class ShockleyDiode(AbstractDiodeModel):
    """Shockley diode model
    https://en.wikipedia.org/wiki/Diode#Shockley_diode_equation
    """
    def setup(self, n=1.0, Is=1e-12, T=300.):
        super().setup()

        # Model specific parameters
        self.add_inward('Is', Is, unit='amp', desc='Diode reverse bias saturation current')
        self.add_inward('T', T, unit='K', desc='Diode temperature')
        self.add_inward('n', n, desc='Diode ideality / quality factor')

    def compute_Rv(self):
        """Shockley diode equation
        https://en.wikipedia.org/wiki/Diode#Shockley_diode_equation
        """
        K = 1.380649e-23  # Boltzmann constant
        q = units._UNIT_LIB.unit_table['e']._factor  # elementary charge
        R_nom = 2.58515563250e10  # nominal resistance at zero current

        Vin = self.elec_in.V
        Vt = K * (self.T / q)  # thermal voltage
        IsExp = Vin / (self.n * Vt)  # euler exponent
        Id = self.Is * (numpy.exp(IsExp) - 1)  # diode current

        self.Rv = R_nom if Id == 0 else Vin / Id


class Diode(System):
    """Regularized diode, with customizable regularization model.
    """
    def setup(self, model: Type[AbstractDiodeModel] = PiecewiseDiode, **options):
        """Regularized diode constructor.

        Arguments:
        ----------------------
        - name [str]: System name
        - model [Type[AbstractDiodeModel], optional]:
            Model class, derived from `AbstractDiodeModel`.
            Defaults to `PiecewiseDiode`.
        - options:
            Optional keyword arguments forwarded to `model` constructor.
        """
        self.set_model(model, **options)

    def set_model(self, cls: Type[AbstractDiodeModel], **options) -> AbstractDiodeModel:
        """Create sub-system `model` of type `cls`.

        Arguments:
        ----------------------
        - cls [Type[AbstractDiodeModel]]:
            Model class, derived from `AbstractDiodeModel`.
        - options:
            Optional keyword arguments forwarded to `cls` constructor.
        """
        error = None
        if not isinstance(cls, type):
            error = TypeError
        elif not issubclass(cls, AbstractDiodeModel):
            error = ValueError
        if error:
            raise error("`model` must be a class derived from `AbstractDiodeModel`")
        try:
            self.pop_child('model')
        except:
            pass
        model = self.add_child(
            cls('model', **options),
            pulling = ['elec_in', 'elec_out', 'Rv'],  # standard interface
        )
        return model
