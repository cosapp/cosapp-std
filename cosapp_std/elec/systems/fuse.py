from cosapp.systems import System
from cosapp_std.elec.ports import ElecPort


class Fuse(System):
    """Fuse component, simulated as a 1e10R resistor when
    the blow current is reached

    Attributes:
        I : float
            Fuse blow current in Ohms
    """

    def setup(self, I=0.):
        self.add_input(ElecPort, 'elec_in')
        self.add_output(ElecPort, 'elec_out')
        self.add_inward('I', I, unit='amp', desc='Fuse blow current')
        self.add_inward('bR', 1.0e10, unit='ohm', desc='Blown fuse resistance')
        self.add_outward('blownState', 0)
        self.add_outward('stepCount', 0)
        self.add_outward('timeState', 0)

    def compute(self):

        if self.timeState != self.time:
            if self.elec_in.I > self.I:
                self.stepCount = self.stepCount + 1

        if self.stepCount == 1:
            self.blownState = 1

        if self.elec_in.I > self.I or self.blownState == 1:
            self.elec_out.V = self.elec_in.V - (self.elec_in.I * self.bR)
            self.elec_out.I = 0.
        else:
            self.elec_out.I = self.elec_in.I
            self.elec_out.V = self.elec_in.V

        self.timeState = self.time
