from cosapp.systems import System
from cosapp_std.elec.ports import ElecPort


class Resistor(System):
    """Resistor component

    Attributes:
        R : float
            Resistance in Ohms
    """

    def setup(self, R=0.):
        self.add_input(ElecPort, 'elec_in')
        self.add_output(ElecPort, 'elec_out')

        self.add_inward('R', abs(float(R)), unit='ohm', desc='Internal resistance')
        self.add_outward('deltaV')

    def compute(self):
        self.elec_out.I = self.elec_in.I
        self.elec_out.V = self.elec_in.V - (self.elec_out.I * self.R)
        self.deltaV = self.elec_in.V - self.elec_out.V
