import pytest
from cosapp_std.elec.systems import Node
from cosapp_std.elec.ports import ElecPort


@pytest.fixture
def node_split():
    node_split = Node('node_split', n_in=1, n_out=2)
    return node_split


@pytest.fixture
def node_join():
    node_join = Node('node_join', n_in=2, n_out=1)
    return node_join


@pytest.mark.parametrize("ctor, expected", [
    (dict(), dict(error=None)),
    (dict(n_in=1, n_out=1), dict()),
    (dict(n_in=4, n_out=1), dict()),
    (dict(n_in=1, n_out=4), dict()),
    (dict(n_in=8, n_out=2), dict()),
    (dict(n_in=2, n_out=5), dict()),
    (dict(n_in=9, n_out=5), dict()),
    (dict(n_in=1.2, n_out=3), dict(n_in=1)),
    (dict(n_in=2, n_out='3'), dict(n_out=3)),
    # Erroneous cases
    (
        dict(n_in=0, n_out=1),
        dict(error=ValueError, match="at least one incoming and one outgoing branch"),
    ),
    (
        dict(n_in=1, n_out=0),
        dict(error=ValueError, match="at least one incoming and one outgoing branch"),
    ),
])
def test_Node_setup(ctor, expected):
    error = expected.get('error', None)

    if error is None:
        node = Node('node', **ctor)
        n_in = expected.get('n_in', ctor.get('n_in', 1))
        n_out = expected.get('n_out', ctor.get('n_out', 1))
        assert node.n_in == n_in
        assert node.n_out == n_out
        assert len(node.incoming) == n_in
        assert len(node.outgoing) == n_out
        assert all(
            isinstance(port, ElecPort)
            for port in node.incoming
        )
        assert all(
            isinstance(port, ElecPort)
            for port in node.outgoing
        )
        # Check that `incoming` and `outgoing` ports cannot be overwitten
        with pytest.raises(TypeError):
            node.incoming[0] = node.outgoing[0]
        with pytest.raises(TypeError):
            node.outgoing[0] = node.incoming[0]
        # Check off-design problem size
        problem = node.get_unsolved_problem()
        if n_out > 1:
            n_unknowns = n_out
            n_equations = n_in
        else:
            n_unknowns = 0
            n_equations = n_in - 1
        assert problem.shape == (n_unknowns, n_equations)

    else:
        with pytest.raises(error, match=expected.get('match', None)):
            Node('node', **ctor)


def test_Node_split_run_once(node_split):
    e_in = node_split.elec_in0
    e_out0 = node_split.elec_out0
    e_out1 = node_split.elec_out1

    e_in.V = 5.
    e_in.I = 7.5

    node_split.run_once()

    assert e_out0.V == e_in.V
    assert e_out1.V == e_in.V
    assert e_in.I == (e_out0.I + e_out1.I)


def test_Node_join_run_once(node_join):
    e_in0 = node_join.elec_in0
    e_in1 = node_join.elec_in1
    e_out = node_join.elec_out0

    e_in0.V = 5.
    e_in1.V = 5.
    e_in0.I = 0.2
    e_in1.I = 0.1

    node_join.run_once()

    assert e_out.V == (e_in0.V + e_in1.V) / 2
    assert e_out.I == (e_in0.I + e_in1.I)
