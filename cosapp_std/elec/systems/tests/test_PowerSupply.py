import pytest
from cosapp_std.elec.systems import PowerSupply


@pytest.fixture
def powersupply():
    powersupply = PowerSupply('powersupply', V=12.)
    return powersupply


def test_PowerSupply_run_once(powersupply):
    powersupply.V = 12.
    powersupply.elec_in.V = 0.
    powersupply.run_once()
    elec_in = powersupply.elec_in
    elec_out = powersupply.elec_out
    assert elec_out.I == elec_in.I
    assert elec_out.V == 12.
