import pytest
from cosapp_std.elec.systems import Resistor


@pytest.fixture
def resistor():
    resistor = Resistor('resistor')
    resistor.R = 2000.
    return resistor


def test_Resistor_run_once(resistor):
    resistor.elec_in.V = 5.
    resistor.elec_in.I = 0.0025
    resistor.run_once()
    elec_in = resistor.elec_in
    elec_out = resistor.elec_out
    assert elec_out.I == elec_in.I
    assert elec_out.V == pytest.approx(0., rel=1e-14)
    assert resistor.deltaV == pytest.approx(elec_in.V - elec_out.V, rel=1e-14)
