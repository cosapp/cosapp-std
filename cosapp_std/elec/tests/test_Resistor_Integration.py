import pytest
from cosapp.systems import System
from cosapp.drivers import NonLinearSolver, NonLinearMethods
from cosapp_std.elec.systems import Resistor, PowerSupply


class ResistorSys(System):
    def setup(self):
        PSU = self.add_child(PowerSupply('PSU', V=5.))
        R1 = self.add_child(Resistor('R1', R=2000.))

        self.connect(PSU.elec_out, R1.elec_in)
        self.connect(R1.elec_out, PSU.elec_in)


@pytest.fixture
def single_calc():
    system_resistor = ResistorSys('system_res')
    system_resistor.add_driver(NonLinearSolver('solver', method=NonLinearMethods.POWELL, verbose=True))
    system_resistor.run_drivers()
    return system_resistor


def test_Resistor_Current(single_calc):
    assert (single_calc.PSU.elec_out.I) == pytest.approx(0.0025, rel=1e-4)


def test_Resistor_Voltage(single_calc):
    assert (single_calc.PSU.elec_in.V) == pytest.approx(0.0, rel=1e-4)
