from cosapp.ports import Port


class FlowPort(Port):
    """
    Standard port for fluid properties
    """
    def setup(self):
        self.add_variable("fluid", "air", dtype=(str, dict), desc="Fluid material")
        self.add_variable("W", 1., unit="kg/s", desc="Mass flow")
