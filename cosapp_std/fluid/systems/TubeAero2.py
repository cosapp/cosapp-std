from cosapp.systems import System
from ..ports import FlowPort2


class TubeAero2(System):
    """Tube component

    pt_out = pt_in - K * W_in**2
    ht_out = ht_in + heat / W_in

    Attributes:
        K : float
            Pressure loss coefficient
        heat : float
            heat exchange
    """

    def setup(self):
        # inputs / outputs
        self.add_input(FlowPort2, 'fl_in')
        self.add_output(FlowPort2, 'fl_out')

        # inwards
        self.add_inward('K', 0.1, unit='Pa / (kg/s)**2', desc='Pressure loss coefficient')
        self.add_inward('heat', 10., unit='W', desc='heat exchange')

        # outwards
        self.add_outward('dP', desc="Pressure Loss")
        self.add_outward('dH', desc='Enthalpy Change')

    def compute(self):
        self.fl_out.copyFromPort(self.fl_in)

        self.dH = self.heat / self.fl_in.MassFlow
        hout = self.fl_in.Enthalpy + self.dH

        self.dP = self.K * self.fl_in.MassFlow * abs(self.fl_in.MassFlow)
        pout = self.fl_in.Pressure - self.dP

        self.fl_out.setStateFromEnthalpy(pout, hout)
