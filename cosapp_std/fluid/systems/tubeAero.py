from cosapp.systems import System
from cosapp_std.fluid.ports import FlowPortTotal


class TubeAero(System):
    """Tube component

    pt_out = pt_in - K * W_in**2
    ht_out = ht_in + heat / W_in

    Attributes:
        K : float
            Pressure loss coefficient
        heat : float
            heat exchange
    """

    def setup(self):
        # inputs / outputs
        self.add_input(FlowPortTotal, 'fl_in')
        self.add_output(FlowPortTotal, 'fl_out')

        # inwards
        self.add_inward('K', 0.1, unit='Pa / (kg/s)**2', desc='Pressure loss coefficient')
        self.add_inward('heat', 10., unit='W', desc='heat exchange')

        # outwards
        self.add_outward('dP', desc="Pressure Loss")
        self.add_outward('dH', desc='Enthalpy Change')

    def compute(self):
        self.fl_out.fluid = self.fl_in.fluid
        self.fl_out.W = self.fl_in.W

        self.dH = self.heat / self.fl_in.W
        self.fl_out.Ht = self.fl_in.Ht + self.dH

        self.dP = self.K * self.fl_in.W * abs(self.fl_in.W)
        self.fl_out.Pt = self.fl_in.Pt - self.dP
