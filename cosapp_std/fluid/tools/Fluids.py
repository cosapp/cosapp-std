""" Several Fluid objects """

import abc   # abstract base class

# The CoolProp fluid implementation is commented out here to avoid requiring a dependency,
# but is usable (after uncommenting the import and class).  CoolProp contains a good implementation
# of multiphase fluid properties, including water, and is open-source.
#import CoolProp.CoolProp as CP    # for FluidCoolProp

class Fluid(abc.ABC):
    """ The Fluid interface defines the functions expected to be implemented by a fluid-library object.
        Fluid objects will be minimal within a system, and be responsible for storing the configuration
        necessary for the fluid property library (e.g. maps fo cp=f(T) and so forth).

        The Fluid object provides a template object (StateCollection) that the FlowPort should use to
        keep track of the actual thermodynamic state.  The Fluid should be used to interpret this
        collection.  This allows fluid-property-library specific ways of storing the thermodynamic state.
    """

    @property
    @abc.abstractmethod
    def FluidName(self):
        pass

    @property
    def FluidKey(self):
        return self._key

    @FluidKey.setter
    def FluidKey(self,key):
        self._key = key


    @abc.abstractmethod
    def getStateCollection(self):
        """ The StateCollection provides a template object to store the thermodynamic state.
            The Fluid class should provide something like:  SimpleNamespace(p=0.0, T=0.0, h=0.0)
        """
        pass

    @property
    @abc.abstractmethod
    def NumConstituents(self):
        pass

    @property
    @abc.abstractmethod
    def ReferenceTemperature(self):
        pass

    @property
    @abc.abstractmethod
    def ReferencePressure(self):
        pass

    @abc.abstractmethod
    def setStateFromEnthalpy(self, state, p, h, xi):
        pass

    @abc.abstractmethod
    def setStateFromTemperature(self, state, p, T, xi):
        pass

    @abc.abstractmethod
    def getPressure(self, state):
        pass
    
    @abc.abstractmethod
    def getTemperature(self, state):
        pass
   
    @abc.abstractmethod
    def getEnthalpy(self, state):
        """Calculates specific enthalpy, kJ/kg"""
        pass
    
    @abc.abstractmethod
    def getDensity(self, state):
        """Calculates density, kg/m**3"""
        pass

    @abc.abstractmethod
    def getInternalEnergy(self, state):
        """Calculates specific internal energy, kJ/kg"""
        pass

    @abc.abstractmethod
    def getThermalConductivity(self, state):
        """Calculates thermal conductivity, k in kW/m/K"""
        pass
    
    @abc.abstractmethod
    def getDynamicViscosity(self, state):
        """ Calculates dynamic viscosity, mu, in Pa*s"""
        pass

    @abc.abstractmethod
    def getGasConstant(self, state):
        """ Returns the gas constant Runiversal/Moleweight.  Not a constant for variable-massfraction mixtures """
        pass

    @abc.abstractmethod
    def getCp(self, state):
        """ Calculates specific heat at constant pressure, cp, in kJ/kg/K"""
        pass

    @abc.abstractmethod
    def getCv(self, state):
        """ Calculates specific heat at constant volume, cv, in kJ/kg/K"""
        pass

    @abc.abstractmethod
    def getSpecificHeatRatio(self, state):
        """ Calculates the ratio of specific heats cp/cv"""
        pass
    
    @abc.abstractmethod
    def getPrandtlNumber(self, state):
        """ Calculates the Prandtl number, momentum/thermal diffusivity, cp*mu/k """
        pass

    @abc.abstractmethod
    def getCoefficientOfThermalExpansion(self, state):
        """ Calculates coefficient of thermal expansion, 1/K"""
        pass

    @abc.abstractmethod
    def getPhase(self, state):
        """ Returns the phase of the state.  S=solid, L=liquid, 2=two-phase liquid/gas, G=gas  """
        pass




class FluidAir(Fluid):
    """
    Implement the fluid property abstraction for dry air
    """
    def __init__(self):
        self._Rgas = 8.3144598 / 28.9647  # unit='kJ/kg/K'
        self._Z = 1.0
        self._cp = 1.0038    # airish kJ/kg/K
        self._Tref = 0.0   # h = cp*(T-Tref)
        self._gamma = 1.400  # cp/cv, airish
        self._key = "Air"
        self._thermalcond = 0.02624e-3  # air, at 27 deg C, 1 atm
        self._visc = 1.846e-5  # Right now, just do air, at 27 deg C, 1 atm
        self._pref = 101325.0
        self._beta = 0.0034    # thermal expansion
    
    @property
    def FluidName(self):
        return "Air"

    def getStateCollection(self):
        from types import SimpleNamespace
        return SimpleNamespace(p=101325.0, T=300.0, h=0.0) 

    @property
    def NumConstituents(self):
        return 1

    @property
    def ReferenceTemperature(self):
        return self._Tref

    @property
    def ReferencePressure(self):
        return self._pref

    def setStateFromEnthalpy(self, state, p, h, xi):
        state.p = p
        state.h = h
        state.T = h / self._cp + self._Tref

    def setStateFromTemperature(self, state, p, T, xi):
        state.p = p
        state.T = T
        state.h = self._cp*(T - self._Tref)

    def getPressure(self, state):
        return state.p

    def getTemperature(self, state):
        return state.T
    
    def getEnthalpy(self, state):
        return state.h
    
    def getDensity(self, state):
        rho = state.p / self._Rgas / state.T / self._Z  / 1000.0 # 
        return rho

    def getInternalEnergy(self, state):
        """Calculates specific internal energy, kJ/kg"""
        cv = self.getCv(state)
        return cv*(state.T - self.ReferenceTemperature)           

    def getThermalConductivity(self, state):
        """Calculates thermal conductivity, k in kW/m/K"""
        return self._thermalcond
    
    def getDynamicViscosity(self, state):
        """ Calculates dynamic viscosity, mu, in Pa*s"""
        return self._visc

    def getGasConstant(self, state):
        """ Returns the gas constant Runiversal/Moleweight.  Not a constant for variable-massfraction mixtures """
        return self._Rgas        

    def getCp(self, state):
        """ Calculates specific heat at constant pressure, cp, in kJ/kg/K"""
        #  Right now, just do air, at 27 deg C, 1 atm
        return self._cp

    def getCv(self, state):
        """ Calculates specific heat at constant volume, cv, in kJ/kg/K"""
        return self.getCp(state) / self.getSpecificHeatRatio(state)

    def getSpecificHeatRatio(self, state):
        """ Calculates the ratio of specific heats cp/cv"""
        #  Right now, just do air, at 27 deg C, 1 atm
        return self._gamma
    
    def getPrandtlNumber(self, state):
        """ Calculates the Prandtl number, momentum/thermal diffusivity, cp*mu/k """
         # Right now, just do air, at 27 deg C, 1 atm
        return self._cp*self._visc/self._thermalcond

    def getCoefficientOfThermalExpansion(self, state):
        """ Calculates coefficient of thermal expansion, 1/K"""
        return self._beta 

    def getPhase(self, state):
        """ Returns the phase of the state.  S=solid, L=liquid, 2=two-phase liquid/gas, G=gas  """
         #  Right now, just do air, at 27 deg C, 1 atm
        return 'G'    



class FluidLiquidWater(Fluid):
    """
    Implement the fluid property abstraction for room temperature water
    """
    def __init__(self):
        self._rho = 997.0  # room temperature, unit='kg/m**3'
        self._cp = 4.186    # kJ/kg/K
        self._Tref = 273.15+20.0   # h = cp*(T-Tref)
        self._pref = 101325.0
        self._key = "Water"
        self._thermalcond = 0.598e-3 # k in kW/m/K at Tref
        self._visc = 8.90e-4 # Pa*s at Tref
        self._beta = 210e-6  # thermal expansion, 1/K, at Tref

    @property
    def FluidName(self):
        return "LiquidWater"

    def getStateCollection(self):
        from types import SimpleNamespace
        return SimpleNamespace(p=101325.0, T=300.0, h=0.0) 

    @property
    def NumConstituents(self):
        return 1

    @property
    def ReferenceTemperature(self):
        return self._Tref

    @property
    def ReferencePressure(self):
        return self._pref

    def setStateFromEnthalpy(self, state, p, h, xi):
        state.p = p
        state.h = h
        state.T = h / self._cp + self._Tref

    def setStateFromTemperature(self, state, p, T, xi):
        state.p = p
        state.T = T
        state.h = self._cp*(T - self._Tref)

    def getPressure(self, state):
        return state.p

    def getTemperature(self, state):
        return state.T
    
    def getEnthalpy(self, state):
        return state.h
    
    def getDensity(self, state):
        return self._rho

    def getInternalEnergy(self, state):
        """Calculates specific internal energy, kJ/kg"""
        cv = self.getCv(state)
        return cv*(state.T - self.ReferenceTemperature)           

    def getThermalConductivity(self, state):
        """Calculates thermal conductivity, k in kW/m/K"""
        return self._thermalcond
    
    def getDynamicViscosity(self, state):
        """ Calculates dynamic viscosity, mu, in Pa*s"""
        return self._visc

    def getGasConstant(self, state):
        """ Returns the gas constant Runiversal/Moleweight.  Not a constant for variable-massfraction mixtures """
        return self._Rgas        

    def getCp(self, state):
        """ Calculates specific heat at constant pressure, cp, in kJ/kg/K"""
        return self._cp

    def getCv(self, state):
        """ Calculates specific heat at constant volume, cv, in kJ/kg/K"""
        return self._cp

    def getSpecificHeatRatio(self, state):
        """ Calculates the ratio of specific heats cp/cv"""
        #  Right now, just do air, at 27 deg C, 1 atm
        return 1.0
    
    def getPrandtlNumber(self, state):
        """ Calculates the Prandtl number, momentum/thermal diffusivity, cp*mu/k """
        return self._cp*self._visc/self._thermalcond

    def getCoefficientOfThermalExpansion(self, state):
        """ Calculates coefficient of thermal expansion, 1/K"""
        return self._beta 

    def getPhase(self, state):
        """ Returns the phase of the state.  S=solid, L=liquid, 2=two-phase liquid/gas, G=gas  """
        return 'L'    




# class FluidCoolProp(Fluid):
#     """
#     Interface to the CoolProp library.
#     Ref:  https://github.com/CoolProp/CoolProp
#     Ref:  http://coolprop.sourceforge.net/index.html
#     License:  MIT

#     should the default be IF97::Water?
#     """
#     def __init__(self, fluidId="Water", asPhase=''):
#         super().__init__()

#         self.HEOS = CP.AbstractState("HEOS", fluidId)
#         self.asPhase = asPhase
#         if asPhase != '':
#             if asPhase == 'gas':
#                 self.HEOS.specify_phase(CP.iphase_gas)
#         self.fluidId = fluidId

#     @property
#     def FluidName(self):
#         return f"{self.fluidId} {self.asPhase}(CoolProp)"

#     def getStateCollection(self):
#         from types import SimpleNamespace
#         return SimpleNamespace(p=101325.0, T=273.15+15.0, h=63.076877)  # water...

#     @property
#     def NumConstituents(self):
#         return 1

#     @property
#     def ReferenceTemperature(self):
#         return 273.15 # ???

#     @property
#     def ReferencePressure(self):
#         return 101325.0   # ???

#     def setStateFromEnthalpy(self, state, p, h, xi):
#         state.p = p
#         state.h = h
#         self.HEOS.update(CP.HmassP_INPUTS, h*1000.0, p)  #CoolProp uses J/kg
#         state.T = self.HEOS.keyed_output(CP.iT)


#     def setStateFromTemperature(self, state, p, T, xi):
#         state.p = p
#         state.T = T
#         self.HEOS.update(CP.PT_INPUTS, p, T)
#         state.h = self.HEOS.keyed_output(CP.iHmass) / 1000.0  # CoolProp uses J/kg

#     def getPressure(self, state):
#         return state.p
    
#     def getTemperature(self, state):
#         return state.T
    
#     def getEnthalpy(self, state):
#         return state.h
    
#     def getDensity(self, state):
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         return self.HEOS.keyed_output(CP.iDmass) 

#     def getInternalEnergy(self, state):
#         """Calculates specific internal energy, kJ/kg"""
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         return self.HEOS.umass() / 1000.0  

#     def getThermalConductivity(self, state):
#         """Calculates thermal conductivity, k in kW/m/K"""
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         k = self.HEOS.keyed_output(CP.iconductivity)  # W/m/K
#         return k/1000.0
    
#     def getDynamicViscosity(self, state):
#         """ Calculates dynamic viscosity, mu, in Pa*s"""
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         mu = self.HEOS.keyed_output(CP.iviscosity)  # Pa*s
#         return mu

#     def getGasConstant(self, state):
#         """ Returns the gas constant Runiversal/Moleweight.  Not a constant for variable-massfraction mixtures """
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         rgas = self.HEOS.keyed_output(CP.igas_constant) / self.HEOS.keyed_output(CP.imolar_mass)
#         return rgas     

#     def getCp(self, state):
#         """ Calculates specific heat at constant pressure, cp, in kJ/kg/K"""
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         cp = self.HEOS.keyed_output(CP.iCpmass)  # 	J/kg/K
#         return cp/1000.0

#     def getCv(self, state):
#         """ Calculates specific heat at constant volume, cv, in kJ/kg/K"""
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         cv = self.HEOS.keyed_output(CP.iCvmass)  # 	J/kg/K
#         return cv/1000.0

#     def getSpecificHeatRatio(self, state):
#         """ Calculates the ratio of specific heats cp/cv"""
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         gamma = self.HEOS.keyed_output(CP.iCpmass) / self.HEOS.keyed_output(CP.iCvmass)
#         return gamma
    
#     def getPrandtlNumber(self, state):
#         """ Calculates the Prandtl number, momentum/thermal diffusivity, cp*mu/k """
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         pr = self.HEOS.keyed_output(CP.iPrandtl)
#         return pr

#     def getCoefficientOfThermalExpansion(self, state):
#         """ Calculates coefficient of thermal expansion, 1/K"""
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         beta = self.HEOS.keyed_output(CP.iisobaric_expansion_coefficient)
#         return beta

#     def getPhase(self, state):
#         """ Returns the phase of the state.  S=solid, L=liquid, 2=two-phase liquid/gas, G=gas  """
#         self.HEOS.update(CP.HmassP_INPUTS, state.h*1000.0, state.p)
#         ip = self.HEOS.keyed_output(CP.iPhase)
#         if ip == CP.iphase_liquid:
#             return 'L'
#         elif ip == CP.iphase_twophase:
#             return '2'
#         else:
#             return 'G'
