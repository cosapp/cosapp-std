from scipy.interpolate import interp1d
from cosapp_std.materials.tools.helpers import param_check, ideal_gas_density, ideal_gas_specific_heat_cp


"""
Hydrogen properties
"""


def molar_mass():
    # kg / mol
    # ref: https://www.angelo.edu/faculty/kboudrea/periodic/structure_mass.htm
    return 1.00797e-03


def adiabatic_index(T=None):
    # ref: White, Frank M. (October 1998). Fluid Mechanics (4th ed.).
    temps = (92.15, 197.15, 293.15, 373.15, 673.15, 1273.15, 2273.15)
    gammas = (1.597, 1.453, 1.410, 1.404, 1.387, 1.358, 1.318)
    gamma_set = interp1d(temps, gammas)
    gamma = float(gamma_set(param_check(T, 'T')))
    return gamma


def density(T=None, P=None):
    # kg / m**3
    return ideal_gas_density(molar_mass(), T, P)


def specific_energy():
    # J / kg
    # ref: College of the Desert, “Module 1, Hydrogen Properties”, Revision 0, December 2001
    # Note that this is the LHV (Lower Heating Value) which considers energy losses such as the energy used to vaporize water
    # The HHV (Higher Heating Value) - perfect combustion would be 1.4186e9 J / kg
    return 1.1993e8


def ideal_gas_specific_heat(T=None):
    # J / kg.K
    # Isobaric specific heat capacity: valid for temperature range 273K - 1800K, maximum error 0.72%
    # Coefficients from B. G. Kyle, Chemical and Process Thermodynamics (Englewood Cliffs, NJ: Prentice-Hall, 1984).
    SHC = {'a': 29.11, 'b': -0.1916e-2, 'c': 0.4003e-5, 'd': -0.8704e-9}
    specific_heat = ideal_gas_specific_heat_cp(molar_mass(), T, SHC)
    return specific_heat
