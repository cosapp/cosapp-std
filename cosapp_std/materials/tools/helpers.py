import logging
from scipy import constants

logger = logging.getLogger(__name__)


def param_check(arg_value=None, property=None):
    if arg_value is None:
        try:
            arg_value = default_condition(property)[2]
            arg_param = default_condition(property)[0]
            arg_units = default_condition(property)[1]
            warn_message = f"Parameter {property} has not been set, using {arg_param} default value of {arg_value}{arg_units}"
            logger.warning(warn_message)

        except TypeError:
            logger.error("Incorrect type given or no property specified")

    return arg_value


def default_condition(property):
    default_conditions = {
        # default material conditions of air used if none passed
        'T': ('Temperature', 'K', 288.15),
        'P': ('Pressure', 'Pa', 101325.),
        'molar_mass': 2.89647e-2,
        'SHC': ('Specific Heat Coefficients, Ideal Gas', '-', {'a': 28.11, 'b': 0.1967e-2, 'c': 0.4802e-5, 'd': -1.966e-9}),
    }

    try:
        default_value = default_conditions[property]
        return default_value

    except KeyError:
        err_message = f"Error: Default property '{property}' does not exist"
        logger.error(err_message)
        pass


def ideal_gas_density(molar_mass, T=None, P=None):
    # kg / m**3
    # reference: https://en.wikipedia.org/wiki/Density_of_air
    R_specific = constants.gas_constant / molar_mass
    density = param_check(P, 'P') / (R_specific * param_check(T, 'T'))
    return density


def ideal_gas_specific_heat_cp(molar_mass, T=None, SHC=None):
    # J / kg.K
    # Valid for temperature range 273K - 1800K, maximum error for quoted gases is 1.46%
    # SHC is a dictionary containing the set of coefficients a to d
    # Formula reference: B. G. Kyle, Chemical and Process Thermodynamics (Englewood Cliffs, NJ: Prentice-Hall, 1984).
    molar_mass = param_check(molar_mass, 'molar_mass')
    T = param_check(T, 'T')
    SHC = param_check(SHC, 'SHC')

    a = SHC['a']
    b = SHC['b']
    c = SHC['c']
    d = SHC['d']

    specific_heat_molar = a + (b * T) + (c * (T**2)) + (d * (T**3))
    specific_heat = specific_heat_molar / molar_mass
    return specific_heat
