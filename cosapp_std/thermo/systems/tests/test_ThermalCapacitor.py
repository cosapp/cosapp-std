from cosapp_std.thermo.systems.thermalCapacitor import ThermalCapacitor
from cosapp.drivers import RungeKutta
# Validation
import pytest


@pytest.fixture
# System Setup
def sys_thermal_capacity():
    thermal_capacity = ThermalCapacitor('capa1')
    return thermal_capacity

@pytest.fixture
def time_calc(sys_thermal_capacity):
    InitialTemp = 25. + 273.15
    InitialHeat = 1000.
    WaterMass = 5.
    WaterCp = 4200.
    deltaTime = 60.

    run1 = sys_thermal_capacity.add_driver(RungeKutta(order=3))
    run1.time_interval = (0., deltaTime)
    run1.dt = 1.

    # define scenario
    run1.set_scenario(values={'thermal_in.T': InitialTemp,
                              'thermal_in.q': InitialHeat,
                              'mass': WaterMass,
                              'cp': WaterCp
                              }
                      )
    
    return sys_thermal_capacity

def test_ThermalCapacitor_run_once(sys_thermal_capacity):
    InitialTemp = 25. + 273.15
    InitialHeat = 1000.
    WaterMass = 5.
    WaterCp = 4200.
    sys_thermal_capacity.thermal_in.T = InitialTemp
    sys_thermal_capacity.thermal_in.q = InitialHeat
    sys_thermal_capacity.mass = WaterMass
    sys_thermal_capacity.cp = WaterCp
    sys_thermal_capacity.run_once()
    thermo_in = sys_thermal_capacity.thermal_in
    thermo_out = sys_thermal_capacity.thermal_out
    assert thermo_out.T == thermo_in.T
    assert thermo_out.q == 0.

def test_ThermalCapacitor_simu(time_calc):
    time_calc.run_drivers()
    assert time_calc.dTdt == pytest.approx(1./21., rel=1e-4)
    assert time_calc.deltaT == pytest.approx(2.857, rel=1e-4)
    assert time_calc.thermal_out.T == pytest.approx(301.00714, rel=1e-4) 
