import pytest
from cosapp_std.thermo.systems import ThermalNode
from cosapp_std.thermo.ports import ThermalPort


@pytest.fixture
def node_split():
    node_split = ThermalNode('node_split', n_in=1, n_out=2)
    return node_split


@pytest.fixture
def node_join():
    node_join = ThermalNode('node_join', n_in=2, n_out=1)
    return node_join


@pytest.mark.parametrize("ctor, expected", [
    (dict(), dict(error=None)),
    (dict(n_in=1, n_out=1), dict()),
    (dict(n_in=4, n_out=1), dict()),
    (dict(n_in=1, n_out=4), dict()),
    (dict(n_in=8, n_out=2), dict()),
    (dict(n_in=2, n_out=5), dict()),
    (dict(n_in=9, n_out=5), dict()),
    (dict(n_in=1.2, n_out=3), dict(n_in=1)),
    (dict(n_in=2, n_out='3'), dict(n_out=3)),
    # Erroneous cases
    (
        dict(n_in=0, n_out=1),
        dict(error=ValueError, match="at least one incoming and one outgoing branch"),
    ),
    (
        dict(n_in=1, n_out=0),
        dict(error=ValueError, match="at least one incoming and one outgoing branch"),
    ),
])
def test_Node_setup(ctor, expected):
    error = expected.get('error', None)

    if error is None:
        node = ThermalNode('node', **ctor)
        n_in = expected.get('n_in', ctor.get('n_in', 1))
        n_out = expected.get('n_out', ctor.get('n_out', 1))
        assert node.n_in == n_in
        assert node.n_out == n_out
        assert len(node.incoming) == n_in
        assert len(node.outgoing) == n_out
        assert all(
            isinstance(port, ThermalPort)
            for port in node.incoming
        )
        assert all(
            isinstance(port, ThermalPort)
            for port in node.outgoing
        )
        # Check that `incoming` and `outgoing` ports cannot be overwitten
        with pytest.raises(TypeError):
            node.incoming[0] = node.outgoing[0]
        with pytest.raises(TypeError):
            node.outgoing[0] = node.incoming[0]
        # Check off-design problem size
        problem = node.get_unsolved_problem()
        if n_out > 1:
            n_unknowns = n_out
            n_equations = n_in
        else:
            n_unknowns = 0
            n_equations = n_in - 1
        assert problem.shape == (n_unknowns, n_equations)

    else:
        with pytest.raises(error, match=expected.get('match', None)):
            ThermalNode('node', **ctor)


def test_Node_split_run_once(node_split):
    t_in = node_split.thermal_in0
    t_out0 = node_split.thermal_out0
    t_out1 = node_split.thermal_out1

    t_in.T = 300.
    t_in.q = 1.5

    node_split.run_once()

    assert t_out0.T == t_in.T
    assert t_out1.T == t_in.T
    assert t_in.q == (t_out0.q + t_out1.q)


def test_Node_join_run_once(node_join):
    t_in0 = node_join.thermal_in0
    t_in1 = node_join.thermal_in1
    t_out = node_join.thermal_out0

    t_in0.T = 300.
    t_in1.T = 300.
    t_in0.q = 0.2
    t_in1.q = 0.1

    node_join.run_once()

    assert t_out.T == (t_in0.T + t_in1.T) / 2
    assert t_out.q == (t_in0.q + t_in1.q)
