import pytest
from cosapp_std.thermo.systems import ThermalResistor


@pytest.fixture
def resistor():
    resistor = ThermalResistor('ThermoResistor')
    thermolambda = 0.07
    A = 1.
    L = 0.03
    resistor.R = L / (thermolambda * A)
    return resistor


def test_ThermalResistor_run_once(resistor):
    resistor.thermal_in.T = 150. + 273.15
    resistor.thermal_in.q = 140.
    resistor.run_once()
    thermal_in = resistor.thermal_in
    thermal_out = resistor.thermal_out
    assert (thermal_out.T - 273.15) == pytest.approx(90., rel=1e-14)
    assert thermal_out.q == thermal_in.q
    
